<?php

namespace App\Models;

use JeffGreco13\FilamentTeams\Models\FilamentTeam;

class Team extends FilamentTeam
{

    /**
     * @var array
     */
    protected $fillable = ["name", "owner_id", "credentials"];


    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'credentials' => 'array',
    ];

}
