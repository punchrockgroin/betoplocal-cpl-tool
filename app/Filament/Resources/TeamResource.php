<?php

namespace App\Filament\Resources;

use App\Filament\Resources\TeamResource\Pages;
use App\Rules\GoogleCredentialImportRule;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\HtmlString;
use JeffGreco13\FilamentTeams\Models\FilamentTeam;
use JeffGreco13\FilamentTeams\Resources\FilamentTeamResource;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Closure;


class TeamResource extends FilamentTeamResource
{

    public static function form(Form $form): Form
    {
        return $form->schema([
            Forms\Components\TextInput::make("name")
                ->label("Team name")
                ->required(),
            Forms\Components\BelongsToSelect::make("owner_id")
                ->searchable()
                ->relationship("owner", "name")
                ->default(auth()->user()->id)
                ->disablePlaceholderSelection(),
            Forms\Components\BelongsToManyMultiSelect::make("users")
                ->label("Team users")
                ->helperText(
                    "No invitation emails will be sent by updating this value."
                )
                ->relationship("users", "name")
                ->default(function(){
                    if (config("filament-teams.sync_owner_as_team_member")){
                        return [auth()->user()->id];
                    }
                })
                ->columnSpan([
                    'sm' => 2,
                    'xl' => 3,
                    '2xl' => 4,
                ]),
            Forms\Components\Repeater::make('credentials')
                ->schema([
                    Forms\Components\Repeater::make('tiktok')
                        ->label(__('Tik-Tok'))
                        ->schema([
                            // ...
                            Forms\Components\TextInput::make('tiktok_access_token')
                                ->label(__('Access Token')),
                        ])
                        ->disableItemCreation()
                        ->disableItemDeletion()
                        ->disableItemMovement()
                        ->columnSpan([
                            'sm' => 2,
                            'xl' => 3,
                            '2xl' => 4,
                        ]),
                    Forms\Components\Repeater::make('facebook')
                        ->label(__('Facebook'))
                        ->schema([
                            // ...
                            Forms\Components\TextInput::make('fb_business_id')->label(__('Business ID')),
                            Forms\Components\TextInput::make('fb_system_user_id')->label(__('System User ID')),
                            Forms\Components\TextInput::make('fb_system_user_key')->label(__('System User Key')),
                        ])
                        ->disableItemCreation()
                        ->disableItemDeletion()
                        ->disableItemMovement()
                        ->columnSpan([
                            'sm' => 2,
                            'xl' => 3,
                            '2xl' => 4,
                        ]),
                    Forms\Components\Repeater::make('monday')
                        ->label(__('Monday'))
                        ->schema([
                            // ...
                            Forms\Components\TextInput::make('monday_token')
                                ->required()
                                ->validationAttribute('Monday API Token')
                                ->label(__('Monday API Token')),
                        ])
                        ->disableItemCreation()
                        ->disableItemDeletion()
                        ->disableItemMovement()
                        ->columnSpan([
                            'sm' => 2,
                            'xl' => 3,
                            '2xl' => 4,
                        ]),
                    Forms\Components\Repeater::make('google')
                        ->label(__('Google'))
                        ->schema([
                            // ...
                            Forms\Components\FileUpload::make('google_credential_path')
                                ->label(__('Google Credentials'))
                                ->required()
                                ->validationAttribute('Google Credentials')
                                ->disk('local')
                                ->preserveFilenames()
                                ->reactive()
                                ->rules([new GoogleCredentialImportRule]),
                            Forms\Components\Placeholder::make('Credential File Info')
                                ->content(function (Closure $get){
                                    if(is_array($get('google_credential_path'))){
                                       if(Storage::disk('local')->exists(head($get('google_credential_path')))){
                                           // Get file from path
                                            $fileData = Storage::disk('local')->get(head($get('google_credential_path')));
                                            // Decode file data
                                            $decodedFileData = json_decode($fileData, true);
                                            // Because there's no real way to validate if the key is valid...
                                            $containsNeededData = Arr::has($decodedFileData, ['client_id', 'client_email', 'private_key']);

                                            if($containsNeededData){
                                                $credentials = Arr::only($decodedFileData, [
                                                    'project_id',
                                                    'client_email',
                                                    'client_email',
                                                ]);

                                                return new HtmlString(view('helpers.googlecredentials', ['credentials' => $credentials])->render());
                                            }
                                        }
                                    }
                                    return new HtmlString('<div class="flex items-center justify-center gap-3 px-3 py-2 rounded-lg font-normal transition bg-primary-500 text-white">Please add a Google Credential</div>');
                                })
                        ])
                        ->disableItemCreation()
                        ->disableItemDeletion()
                        ->disableItemMovement()
                        ->columnSpan([
                            'sm' => 2,
                            'xl' => 3,
                            '2xl' => 4,
                        ])
                ])
                ->columnSpan([
                    'sm' => 2,
                    'xl' => 3,
                    '2xl' => 4,
                ])
                ->disableItemCreation()
                ->disableItemDeletion()
                ->disableItemMovement()
        ]);
    }

    public static function getPages(): array
    {
        return [
            "index" => Pages\ListTeams::route("/"),
            "create" => Pages\CreateTeam::route("/create"),
            "edit" => Pages\EditTeam::route("/{record}/edit"),
        ];
    }
}
