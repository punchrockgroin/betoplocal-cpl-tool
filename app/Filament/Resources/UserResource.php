<?php

namespace App\Filament\Resources;

use App\Filament\Resources\UserResource\Pages;
use App\Filament\Resources\UserResource\RelationManagers;
use App\Models\User;
use Filament\Forms;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use STS\FilamentImpersonate\Impersonate;

class UserResource extends Resource
{
    protected static ?string $model = User::class;

    protected static ?string $navigationIcon = 'heroicon-o-collection';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                //
                Forms\Components\Select::make("current_team_id")
                    ->label("Current team")
                    ->options(fn($record) => $record->teams()->pluck("name", "id")),
                Forms\Components\BelongsToManyMultiSelect::make("teams")
                    ->relationship("teams", "name"),
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                Tables\Columns\TextColumn::make('name')->sortable(),
                Tables\Columns\TextColumn::make('email')->sortable(),
                Tables\Columns\BadgeColumn::make("current_team_id")->label(
                    "Current team"
                )->getStateUsing(fn($record) => $record->currentTeam?->name),
                Tables\Columns\TagsColumn::make("teams")->getStateUsing(
                    fn($record) => $record
                        ->teams()
                        ->pluck("name")
                        ->toArray()
                )
            ])
            ->filters([
                //
            ])
            ->prependActions([
                Impersonate::make('impersonate'), // <---
            ])
            ->defaultSort('name');
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListUsers::route('/'),
            'create' => Pages\CreateUser::route('/create'),
            'edit' => Pages\EditUser::route('/{record}/edit'),
        ];
    }
}
