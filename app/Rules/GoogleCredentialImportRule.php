<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Arr;
use Livewire\TemporaryUploadedFile;

class GoogleCredentialImportRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  TemporaryUploadedFile  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $value->getRealPath();

        // Get file from path
        $fileData = $value->get();
        // Decode file data
        $decodedFileData = json_decode($fileData, true);
        // Because there's no real way to validate if the key is valid...
        $containsNeededData = Arr::has($decodedFileData, ['client_id', 'client_email', 'private_key']);

        if($containsNeededData){
            return true;
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The Google Credentials file appears to be invalid.';
    }
}
